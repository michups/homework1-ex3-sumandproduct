package com.michups.sumandproduct;

/**
 * Created by michups on 22.05.17.
 */
public class SumAndProduct {
    public static void main(String[] args) {
        int[] input = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        int sum=0;
        int product = 1;
        System.out.print("Input numbers: ");


        for (int i: input    ) {
            System.out.print(i + ",");
            sum+=i;
            product*=i;
        }
        System.out.println();
        System.out.println("Product: "+product);
        System.out.println("Sum: "+sum);
    }

}
